(function() {
    var CanvasJS = window.CanvasJS || CanvasJS ? window.CanvasJS : null;
    if (CanvasJS && CanvasJS.Chart) {
        CanvasJS.Chart.prototype.updateCustomOptions = function() {
            this.animationComplete = this.options.animationComplete ? this.options.animationComplete : null;
        }
        var chartRender = CanvasJS.Chart.prototype.render;
        CanvasJS.Chart.prototype.render = function(options) {
            var _this = this;
            var render = chartRender.apply(this, arguments);
            this.updateCustomOptions();
            this.addEventListener("dataAnimationEnd", function() {
                _this.dispatchEvent("animationComplete", {
                    chart: _this,
                    options: _this.options
                }, _this.chart);
            });
            return render;
        }
    }
})();
